<?php
    $fn = $_POST['fn']; 
    $ln = $_POST['ln'];
    $un = $_POST['un'];
    $pwd = $_POST['pwd']; 
    $ag = $_POST['ag'];
    $em = $_POST['em'];
//get fn and others from front.html  

    if(strlen($fn) == 0){ //use strlen function to check length of the alphabet
        echo "Forename must not be blank <br>"; 
    }elseif(strlen($fn) < 3 ){
        echo "Forename must be more that 3 characters <br>";
    }elseif(strpos($fn, ' ')){ //use strpos to find where is the space
        echo "Forename must not have space <br>";
    }else{
        echo "Forename is valid! <br>";
    } 

    if(strlen($ln) == 0){ //use strlen function to check length of the alphabet
        echo "Surname must not be blank <br>";
    }elseif(strlen($ln) < 3 ){
        echo "Surname must be more than 3 characters <br>";
    }elseif(strpos($ln, ' ')){ //use strpos to find where is the space
        echo "Surname must not have space <br>";
    }else{
        echo "Surname is valid! <br>";
    } 

    if(strlen($ln) < 5 ){
        echo "Username must be more than 3 characters <br>";
    }else{
        echo "Username is valid! <br>";
    }

    if($ag < 18 || $ag > 110){
        echo "Age must be between 18-110 <br>";
    }else{
        echo "Age is valid ! <br>";
    }

    if(strlen($pwd) < 8 ){
        echo "Password must be more than 8 characters <br>";
    }elseif(!preg_match('/[a-z]/',$pwd) || !preg_match('/[A-Z]/',$pwd) || !preg_match('/[0-9]/',$pwd)|| !preg_match('/[#$%&!@|=+-?*£]/',$pwd) ){ //preg_match function use to check if its contain that characters or not
        echo "Password must containing both upper and lower case letters, numbers and symbols <br>" ;
    }else{
        echo "Password is valid <br>";
    }

    //email is already check by type email in input tag


    
?>